# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer:
pkgname=cyclone
pkgver=0.31.0
pkgrel=1
pkgdesc="A compiler for application development with R7RS Scheme"
url="https://justinethier.github.io/cyclone/"
# mips64: crashes during compilation of first Scheme file with cyclone
# s390x: ck on s390x does not have ck_pr_cas_8 (used by cyclone)
arch="all !mips64 !s390x"
license="MIT"
makedepends="ck-dev cyclone-bootstrap"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/justinethier/cyclone/archive/refs/tags/v$pkgver.tar.gz
	0001-Flush-current-output-port-after-writing-prompt-to-it.patch
	0002-Don-t-use-conditional-assignment-operator-for-CFLAGS.patch"

export PREFIX=/usr
export DATADIR=$PREFIX/lib/cyclone

# Since cyclone itself is written in Scheme it needs to be bootstraped.
# For this propose, upstream provides a dedicated cyclone-bootstrap
# repository which includes C files generated by a previous cyclone
# version. We package cyclone-bootstrap in a separate package and
# compile the original cyclone Scheme sources using it. This is useful
# to backport patches from upstream without having to patch
# compiler-generated C files.
#
# It should also be possible to use provides and a recursive dependency
# for bootstraping in the future. Like we do with rust and others.

# Flags to use build cyclone compiler, instead of cyclone-bootstrap.
_local_cyclone="./cyclone -I . -COPT '-Iinclude' -CLNK '-L.'"

prepare() {
	default_prepare

	# Disable test failing on armhf, armv7 and ppc64le for now.
	# See https://github.com/justinethier/cyclone/issues/475
	sed -i ./tests/srfi-143-tests.scm -e '/test-167/d'
}

build() {
	# Build the cyclone compiler using cyclone-bootstrap.
	PATH="$PATH:/usr/lib/cyclone-bootstrap/bin" make -j1 cyclone

	# Using the compiler from the previous step, build everything else.
	# Build libraries first to ensure that we can use these instead
	# of the ones provided by cyclone-bootstrap to build the tools.
	make -j1 CYCLONE="$_local_cyclone" libs
	make -j1 CYCLONE="$_local_cyclone" all
}

check() {
	make -j1 CYCLONE="$_local_cyclone" test
}

package() {
	DESTDIR="$pkgdir" make -j1 install
}

dev() {
	# Custom dev to not move Cyclone object files.
	# Allows using Cyclone without installing -dev.
	#
	# The -dev subpackage should only be needed for FFI.
	amove /usr/include /usr/lib/libcyclone*
}

sha512sums="
2671dab73a519c3d7dd49ff820a5137bd5bc90d6313e80874941d59eea9e10436b9b71dfcd7ab79b014c26e259e1ef0a4472d84a3d3514c48222bc24a2da766f  cyclone-0.31.0.tar.gz
7d03d91ca50ff23da518ad3e65da5e957b9d5f2d338748c22a9fde59c6d684b90cea2c3f43f390f11bf894a37ccdd53704686d26ac093bcd97fcb557a6f910a2  0001-Flush-current-output-port-after-writing-prompt-to-it.patch
205d0931b36c473ad104ece71f27e3e61de557a4faa5f3bb5ee30c16e87b8cb2a429b1d3945b5e489a111e029d030a32f3304ac283cd605135f16d96fb421267  0002-Don-t-use-conditional-assignment-operator-for-CFLAGS.patch
"
