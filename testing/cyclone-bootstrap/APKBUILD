# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer:
pkgname=cyclone-bootstrap
pkgver=0.31.0
pkgrel=1
pkgdesc="Cyclone version used to bootstrap cyclone compiler"
url="https://justinethier.github.io/cyclone/"
# s390x: ck on s390x does not have ck_pr_cas_8 (used by cyclone)
arch="all !s390x"
license="MIT"
depends="!cyclone"
makedepends="ck-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/justinethier/cyclone-bootstrap/archive/refs/tags/v$pkgver.tar.gz
	fix-cflags.patch
	tests-without-install.patch"

# This package is only intended for bootstraping the cyclone package. As
# such, binaries are not installed to /usr/bin but to a custom location.

export PREFIX=/usr
export BINDIR=$PREFIX/lib/cyclone-bootstrap/bin
export DATADIR=$PREFIX/lib/cyclone

build() {
	make
}

check() {
	make test
}

package() {
	DESTDIR="$pkgdir" make install
}

sha512sums="
f0a5d7c6890183ccab65028f43be2080dcf7d44b80a254f176ffa034c04a2701266febbc3f91072e58175bc84eb86a839dab1dbb3674868128ef3d8e650c91c9  cyclone-bootstrap-0.31.0.tar.gz
e22b5afb4388ef87198542372489df0e5eed49d540c4f5c8700c35cc986f2be1fa1a70773b40b8681d39ab15d31b6472439f389b6c3bd0cf1e060556807dd7f1  fix-cflags.patch
3fa9afc02bbb3d8f2b6d3b09ee1112312b5f747cdf3722c5de02d36fe1f9b0880bc8df932273b89745b091f23248249301fbfd687627e3290b17451c1d190446  tests-without-install.patch
"
