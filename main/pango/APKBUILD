# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=pango
pkgver=1.48.7
pkgrel=0
pkgdesc="library for layout and rendering of text"
url="https://www.pango.org/"
arch="all"
license="LGPL-2.1-or-later"
depends_dev="pango-tools=$pkgver-r$pkgrel"
makedepends="meson cairo-dev expat-dev gobject-introspection-dev help2man
	fontconfig-dev glib-dev harfbuzz-dev libxft-dev fribidi-dev
	gtk-doc"
checkdepends="ttf-dejavu ttf-cantarell ttf-droid ttf-tlwg"
install="$pkgname.pre-deinstall"
triggers="$pkgname.trigger=/usr/lib/pango/*/modules"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-doc $pkgname-tools"
source="https://download.gnome.org/sources/pango/${pkgver%.*}/pango-$pkgver.tar.xz
	fix-x86.patch
	"

# secfixes:
#   1.44.1-r0:
#     - CVE-2019-1010238

build() {
	abuild-meson \
		-Dintrospection=enabled \
		-Dgtk_doc=false \
		build
	meson compile ${JOBS:+-j ${JOBS}} -C build
}

check() {
	meson test --no-rebuild -v -C build
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C build
}

tools() {
	pkgdesc="$pkgdesc (tools)"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/* "$subpkgdir"/usr/bin
}

sha512sums="
e5680d628f531bea7633945bd74dc51a93c4c881dec58fcf9f6271043e77b82a4805e311355cb4d9f8cdfcdf52785a5a6391517a264b62416c26e82dff4aca39  pango-1.48.7.tar.xz
9eed9b73b8321c3c870c1c78c4fa1efb2e60c8659102a7c9345eeb53c2ae287a7a9f46291ab481a730efc21b899498dc9616b5fd4848dac75965909807e9989c  fix-x86.patch
"
