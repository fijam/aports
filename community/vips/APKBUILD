# Contributor: Will Jordan <will.jordan@gmail.com>
# Maintainer: Will Jordan <will.jordan@gmail.com>
pkgname=vips
pkgver=8.11.2
pkgrel=2
pkgdesc="fast image processing library with low memory needs"
url="https://libvips.github.io/libvips/"
# ppc64le blocked by failing test: test_seq.sh
# mips64 blocked by missing openexr-dev
arch="all !ppc64le !mips64"
license="LGPL-2.1-or-later"
makedepends="
	cfitsio-dev
	expat-dev
	fftw-dev
	giflib-dev
	glib-dev
	gobject-introspection-dev
	lcms2-dev
	libexif-dev
	libheif-dev
	libimagequant-dev
	libjpeg-turbo-dev
	libpng-dev
	libwebp-dev
	openexr-dev
	openjpeg-dev
	orc-dev
	pango-dev
	poppler-dev
	tiff-dev
	"
checkdepends="bc"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-poppler $pkgname-tools"
source="https://github.com/libvips/libvips/releases/download/v$pkgver/vips-$pkgver.tar.gz"

case "$CARCH" in
	s390x|riscv64|mips64) ;;
	*) makedepends="$makedepends librsvg-dev" ;;
esac

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-debug=no \
		--without-gsf \
		--disable-static \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--docdir=/usr/share/doc \
		--enable-introspection
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="Command-line tools for $pkgname"

	amove usr/bin
}

poppler() {
	pkgdesc="Poppler support for $pkgname"

	amove usr/lib/vips-modules-${pkgver%.*}/vips-poppler.so
}

sha512sums="
2212074a1e2fc9b18af977c32460b8f35fb3476ff302c7b05fa696c2e77d80c5beba5a3d9006a4dd27bbe00ce253518dff0e50b28098e72b516f754fffb4eae9  vips-8.11.2.tar.gz
"
